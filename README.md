# Repo echinen Helm Charts
## Como utilizar o repositório remoto
### Pré-requisito
No Local onde você executará o client do helm é necessário:

- que o plugin do S3 esteja instalado
```
helm plugin install https://github.com/hypnoglow/helm-s3.git
helm3 plugin install https://github.com/hypnoglow/helm-s3.git
```
- ter acesso de read ao bucket (S3) echinen-helm-charts (região de Virginia), credenciais:
```
AKIAS64LHUXWKRRUIY6G
******
```

### Adicionar o repositório remoto
```
AWS_REGION=us-east-1 AWS_ACCESS_KEY_ID=AKIAS64LHUXWKRRUIY6G AWS_SECRET_ACCESS_KEY=****** helm repo add echinen-stable s3://echinen-helm-charts/stable
AWS_REGION=us-east-1 AWS_ACCESS_KEY_ID=AKIAS64LHUXWKRRUIY6G AWS_SECRET_ACCESS_KEY=****** helm3 repo add echinen-stable s3://echinen-helm-charts/stable
```
OBS.: Para obter a Secret Key, entre em contato com elvis@geekguysolutions.com.br

### Listar todos charts do repositório:
```
helm search --regexp 'echinen-stable'
helm3 search repo --regexp 'echinen-stable'
```

### Atualizar todos repositórios de chart helm:
```
helm repo update
helm3 repo update
```
