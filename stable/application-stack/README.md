## Configuration

The following table lists the configurable parameters of the `application-stack` chart and their default values.

| Parameter | Description   | Default   |
|-----------|---------------|-----------|
| `environment` | Set container environment variable `ENVIRONMENT`  | `local`   |
| `ports.tcp.application`   | Specifies application port    | `80`    |
| `ports.udp`   | Specifies upd ports   | `{}`  |
| `service.enabled` | Service enabled?  | `true`    |
| `service.type`    | Service type  | `ClusterIP`   |
| `service.annotations` | Service annotations   | `{}`  |
| `service.nodePorts`   | Service nodePorts custom  | `{}`  |
| `service.extraPorts`  | Service additional ports custom   | `[]`  |
| `ingress.enabled` | Ingress enabled?  | `false`   |
| `ingress.annotations` | Ingress additional annotations    | `{}`  |
| `ingress.hosts`   | Ingress Hostnames | `[]`  |
| `ingress.paths`   | Ingress Paths | `/`  |
| `ingress.portsMapping`    | Ingress ports mapping between Paths and servicePort   | `{}`  |
| `ingress.tls` | Ingress TLS enabled?  | `false`   |
| `deployment.enabled`  | Deployment enabled?   | `true`    |
| `customCommand`   | Custom Command for container  | `[]`  |
| `customArgs`  | Custom Args for container | `[]`  |
| `terminationGracePeriodSeconds`   | Specifies termination grace period in seconds | `30`  |
| `minReplicas` | Minimum number of pods    | 2 |
| `resources.limits.cpu`    | Specifies cpu limit   | `400m`    |
| `resources.limits.memory` | Specifies memory limit    | `400Mi`   |
| `resources.requests.cpu`  | Specifies initial cpu request | `200m` |
| `resources.requests.memory`   | Specifies initial memory request  | `200Mi`    |
| `image.repository`    | Container repository  |  `null`   |
| `image.tag`   | Container tag |  `web` |
| `image.pullPolicy`    | Container pull policy | `Always`  |
| `image.pullSecret`    | Container pull policy | `[]`  |
| `extraEnv`    | Additional container environment variables    | `[]`  |
| `extraContainers` | Additional containers custom  | `[]`  |
| `podAnnotations`  | Pod Annotations   | `[]`  |
| `initContainers`  | Init containers   | `[]`  |
| `volumes` | Specifies Pod volumes | `[]`  |
| `volumeMounts`    | Specifies where Kubernetes will mount Pod volumes | `[]`  |
| `podAntiAffinity` | podAntiAffinity   | `soft`    |
| `nodeSelector` | Specifies nodeSelector | `{}`  |
| `tolerations` | Specifies tolerations | `[]`  |
| `readinessProbe.httpHeaders` | Specifies HTTP headers readiness probe | `[]`  |
| `readinessProbe.path` | Specifies path readiness probe |  `/health-check/readiness`    |
| `readinessProbe.port` | Specifies port readiness probe    |  `4444`    |
| `readinessProbe.initialDelaySeconds`  | Delay before readiness probe is initiated | 5 |
| `readinessProbe.timeoutSeconds`   | When the probe times out   | 3  |
| `readinessProbe.periodSeconds`    | How often to perform the probe    | 10    |
| `readinessProbe.failureThreshold` | Minimum consecutive failures for the probe to be considered failed after having succeeded.    | 3 |
| `readinessProbe.successThreshold` | Minimum consecutive successes for the probe to be considered successful after having failed.  | 1 |
| `livenessProbe.httpHeaders` | Specifies HTTP headers liveness probe | `[]`  |
| `livenessProbe.path` | Specifies path liveness probe |  `/health-check/liveness`    |
| `livenessProbe.port`  | Specifies port liveness probe |  `4444`   |
| `livenessProbe.initialDelaySeconds`  | Delay before liveness probe is initiated | 5 |
| `livenessProbe.timeoutSeconds`   | When the probe times out   | 3  |
| `livenessProbe.periodSeconds`    | How often to perform the probe    | 10    |
| `livenessProbe.failureThreshold` | Minimum consecutive failures for the probe to be considered failed after having succeeded.    | 3 |
| `livenessProbe.successThreshold` | Minimum consecutive successes for the probe to be considered successful after having failed.  | 1 |
| `hpa` | Horizontal Pod Autoscaler (HPA) enable?   | `{}`  |
| `hpa.maxReplicas` | Maximum number of pods    | `null`    |
| `hpa.type.resource.metric`    | Metric Name (type resource) to scale pods | `null`    |
| `hpa.type.resource.targetAverage` | Metric Threshold (type resource) to scale pods    | `null`    |
| `hpa.type.pods.metric`    | Metric Name (type pods) to scale pods | `null`    |
| `hpa.type.pods.targetAverage` | Metric Threshold (type pods) to scale pods    | `null`    |
| `hpa.type.external.metric`    | Metric Name (type external) to scale pods | `null`    |
| `hpa.type.external.metricSelector`    | Metric Selector (type external) to scale pods | `null`    |
| `hpa.type.external.targetAverage` | Metric Threshold (type external) to scale pods    | `null`    |
| `configmaps`  | Enables configmaps    | `[]`  |
| `configmaps.nameSufix`    | Specifies sufix name configmap    | `null`    |
| `configmaps.data` | Specifies datas the configmap | `null`    |
| `serviceMonitor.enabled`  | Prometheus ServiceMonitor enabled?    | `false`   |
| `serviceMonitor.additionalLabels` | Prometheus ServiceMonitor additional labels   | `{}`  |
| `serviceMonitor.scrapeInterval`   | Prometheus ServiceMonitor scrape interval | `30s` |
| `serviceMonitor.port`   | Prometheus ServiceMonitor port | `monitoring` |
| `serviceMonitor.path`   | Prometheus ServiceMonitor path | `/metrics` |
| `cronjobs`    | Enables cronjobs  | `{}`  |
| `cronjobs.nodeSelector`   | Specifies nodeSelector in cronjob | `{}`  |
| `cronjobs.tolerations`    | Specifies tolerations in cronjob  | `[]`  |
| `cronjobs.schedules`  | Schedule list of cronjob  | `[]`  |
| `cronjobs.schedules.name` | Name sufix of cronjob | `{{ .Release.Name }}-name`    |
| `cronjobs.schedules.name.scheduleTime`    | The schedule in Cron format, see https://en.wikipedia.org/wiki/Cron   | `*/1 * * * *` |
| `cronjobs.schedules.name.suspend` | Suspend schedule cronjob  | `false`   |
| `cronjobs.schedules.name.concurrencyPolicy`   | Specifies how to treat concurrent executions of a Job | `Forbid`  |
| `cronjobs.schedules.name.successfulJobsHistoryLimit`  | The number of successful finished jobs to retain  | `1`   |
| `cronjobs.schedules.name.failedJobsHistoryLimit`  | The number of failed finished jobs to retain  | `1`   |
| `cronjobs.schedules.name.restartPolicy`   | Restart policy for all containers within the pod  | `Never`   |
| `cronjobs.schedules.name.serviceAccountName`  | Set name of the ServiceAccount    | `[]`  |
| `cronjobs.schedules.name.resources`   | Compute Resources required    | `default`  |
| `cronjobs.schedules.name.image`   | Docker image name | `null`    |
| `cronjobs.schedules.name.command` | Entrypoint array. Not executed within a shell. The docker image's ENTRYPOINT is used if this is not provided  | `[]`  |
| `cronjobs.schedules.name.args`    | Arguments to the entrypoint   | `[]`  |


Specify each parameter using the `--set key=value[,key=value]` argument to `helm upgrade --install`.
