image:
  repository: grafana/loki
  tag: 2.0.0
  pullPolicy: IfNotPresent

  ## Optionally specify an array of imagePullSecrets.
  ## Secrets must be manually created in the namespace.
  ## ref: https://kubernetes.io/docs/tasks/configure-pod-container/pull-image-private-registry/
  ##
  # pullSecrets:
  #   - myRegistryKeySecretName

ingress:
  enabled: false
  annotations: {}
    # kubernetes.io/ingress.class: nginx
    # kubernetes.io/tls-acme: "true"
  hosts:
    - host: chart-example.local
      paths: []
  tls: []
  #  - secretName: chart-example-tls
  #    hosts:
  #      - chart-example.local

## Affinity for pod assignment
## ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#affinity-and-anti-affinity
affinity:
  podAntiAffinity:
    requiredDuringSchedulingIgnoredDuringExecution:
    - labelSelector:
        matchExpressions:
        - key: app
          operator: In
          values:
          - loki
      topologyKey: "kubernetes.io/hostname"

## StatefulSet annotations
annotations: {}

# enable tracing for debug, need install jaeger and specify right jaeger_agent_host
tracing:
  jaegerAgentHost:

config:
  auth_enabled: false
  ingester:
    chunk_idle_period: 3m
    chunk_block_size: 262144
    chunk_retain_period: 1m
    max_transfer_retries: 0
    lifecycler:
      ring:
        kvstore:
          store: inmemory
        replication_factor: 1
  limits_config:
    enforce_metric_name: false
    reject_old_samples: true
    reject_old_samples_max_age: 216h
    ingestion_rate_mb: 200
  schema_config:
    configs:
    - from: 2021-01-03
      store: aws
      object_store: s3
      schema: v11
      index:
        prefix: k8s_${ENVIRONMENT}_loki_index_
        period: 72h
        # tags:
        #   Environment: ${ENVIRONMENT}
        #   Project: Loki
  server:
    http_listen_port: 3100
  storage_config:
    index_queries_cache_config:
      redis:
        endpoint: redis.kube-logging.svc.cluster.local:6379
        db: 0
        timeout: 15s
        expiration: 1h
    aws:
      s3: s3://${AWS_REGION_S3}/${CLIENTE}-loki-${ENVIRONMENT}
      dynamodb:
        dynamodb_url: dynamodb://${AWS_REGION_DYNAMODB}
        metrics:
          url: http://prometheus-prometheus.kube-monitoring.svc.cluster.local:9090/prometheus
          target_queue_length: 100000
          scale_up_factor: 1.3
          ignore_throttle_below: 1
          queue_length_query: 'sum(avg_over_time(cortex_ingester_flush_queue_length{job="loki-headless"}[2m]))'
          write_throttle_query: 'sum(rate(cortex_dynamo_throttled_total{operation="DynamoDB.BatchWriteItem"}[1m])) by (table) > 0'
          write_usage_query: 'sum(rate(cortex_dynamo_consumed_capacity_total{operation="DynamoDB.BatchWriteItem"}[15m])) by (table) > 0'
          read_usage_query: 'sum(rate(cortex_dynamo_consumed_capacity_total{operation="DynamoDB.QueryPages"}[1h])) by (table) > 0'
          read_error_query: 'sum(increase(cortex_dynamo_failures_total{operation="DynamoDB.QueryPages",error="ProvisionedThroughputExceededException"}[1m])) by (table) > 0'
  chunk_store_config:
    max_look_back_period: 0s
    chunk_cache_config:
      redis:
        endpoint: redis.kube-logging.svc.cluster.local:6379
        db: 1
        timeout: 15s
        expiration: 1h
    write_dedupe_cache_config:
      redis:
        endpoint: redis.kube-logging.svc.cluster.local:6379
        db: 2
        timeout: 15s
        expiration: 1h
  table_manager:
    retention_deletes_enabled: true
    retention_period: 720h
    index_tables_provisioning:
      provisioned_write_throughput: 20
      provisioned_read_throughput: 10
      inactive_write_throughput: 1
      inactive_read_throughput: 5
      write_scale:
        enabled: true
        role_arn: arn:aws:iam::${AWS_ACCOUNT_ID}:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable
        min_capacity: 20
        max_capacity: 1000
        out_cooldown: 60
        in_cooldown: 300
        target: 80
      read_scale:
        enabled: true
        role_arn: arn:aws:iam::${AWS_ACCOUNT_ID}:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable
        min_capacity: 10
        max_capacity: 1000
        out_cooldown: 60
        in_cooldown: 300
        target: 80
      inactive_read_scale:
        enabled: true
        role_arn: arn:aws:iam::${AWS_ACCOUNT_ID}:role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable
        min_capacity: 5
        max_capacity: 1000
        out_cooldown: 60
        in_cooldown: 300
        target: 80

## Additional Loki container arguments, e.g. log level (debug, info, warn, error)
extraArgs: {}
  # log.level: debug

livenessProbe:
  httpGet:
    path: /ready
    port: http-metrics
  initialDelaySeconds: 45

## ref: https://kubernetes.io/docs/concepts/services-networking/network-policies/
networkPolicy:
  enabled: false

## The app name of loki clients
client:
  name: promtail

## ref: https://kubernetes.io/docs/concepts/storage/persistent-volumes/
## If you set enabled as "True", you need :
## - create a pv which above 10Gi and has same namespace with loki
## - keep storageClassName same with below setting
persistence:
  enabled: true
  accessModes:
  - ReadWriteOnce
  size: 10Gi
  annotations: {}
  # selector:
  #   matchLabels:
  #     app.kubernetes.io/name: loki
  # subPath: ""
  # existingClaim:

## Pod Labels
podLabels: {}

## Pod Annotations
podAnnotations:
  prometheus.io/scrape: "true"
  prometheus.io/port: "http-metrics"
  iam.amazonaws.com/role: arn:aws:iam::${AWS_ACCOUNT_ID}:role/kube2iam-${ENVIRONMENT}-loki

podManagementPolicy: OrderedReady

## Assign a PriorityClassName to pods if set
# priorityClassName:

rbac:
  create: true
  pspEnabled: true

readinessProbe:
  httpGet:
    path: /ready
    port: http-metrics
  initialDelaySeconds: 45

replicas: 1

resources:
  limits:
    cpu: 2000m
    memory: 2Gi
  requests:
    cpu: 500m
    memory: 1Gi

securityContext:
  fsGroup: 10001
  runAsGroup: 10001
  runAsNonRoot: true
  runAsUser: 10001

service:
  type: ClusterIP
  nodePort:
  port: 3100
  annotations: {}
  labels: {}

serviceAccount:
  create: true
  name:
  annotations: {}

terminationGracePeriodSeconds: 4800

## ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/
nodeSelector: {}
#   node/reserved-for: system

## Tolerations for pod assignment
## ref: https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/
tolerations: []
# - effect: NoSchedule
#   key: node/reserved-for
#   operator: Equal
#   value: system

# The values to set in the PodDisruptionBudget spec
# If not set then a PodDisruptionBudget will not be created
podDisruptionBudget: {}
# minAvailable: 1
# maxUnavailable: 1

updateStrategy:
  type: RollingUpdate

serviceMonitor:
  enabled: true
  interval: ""
  additionalLabels:
    servicemonitor-discovery: prometheus
  annotations: {}
  # scrapeTimeout: 10s

initContainers: []
## Init containers to be added to the loki pod.
# - name: my-init-container
#   image: busybox:latest
#   command: ['sh', '-c', 'echo hello']

extraContainers: []
## Additional containers to be added to the loki pod.
# - name: reverse-proxy
#   image: angelbarrera92/basic-auth-reverse-proxy:dev
#   args:
#     - "serve"
#     - "--upstream=http://localhost:3100"
#     - "--auth-config=/etc/reverse-proxy-conf/authn.yaml"
#   ports:
#     - name: http
#       containerPort: 11811
#       protocol: TCP
#   volumeMounts:
#     - name: reverse-proxy-auth-config
#       mountPath: /etc/reverse-proxy-conf


extraVolumes: []
## Additional volumes to the loki pod.
# - name: reverse-proxy-auth-config
#   secret:
#     secretName: reverse-proxy-auth-config

## Extra volume mounts that will be added to the loki container
extraVolumeMounts: []

extraPorts: []
## Additional ports to the loki services. Useful to expose extra container ports.
# - port: 11811
#   protocol: TCP
#   name: http
#   targetPort: http

# Extra env variables to pass to the loki container
env: []
