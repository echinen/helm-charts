# Install or Upgrade chart
## Variables
service_name: service name you want to monitor

cliente:
- geekguy

environment:
- hlg
- prod

```helm3 upgrade --install servicemonitor-${service_name} -f ${cliente}/${environment}/values-${service_name}.yaml . --namespace kube-monitoring```

# Scripts
### ./scripts/install-all-sm.sh:
Script utilizado para instalar/atualizar todos os ServicesMonitor de um determinado cliente e ambiente