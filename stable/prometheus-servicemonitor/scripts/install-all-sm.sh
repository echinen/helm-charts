#!/bin/sh

if [ -z $1 ] || [ -z $2 ]; then
    echo 'try run: ./scripts/install-all-sm.sh ${cliente} ${environment}'
    echo 'example: ./scripts/install-all-sm.sh geekguy prod'
else
    CLUSTER=$1
    ENV=$2
    VALUES_PATH="$CLUSTER/$ENV"
    VALUES_FILE=`ls $VALUES_PATH`

    for i in $VALUES_FILE; do
        RELEASE_NAME=`echo $i | cut -d'.' -f1 | sed -e s/^values-/servicemonitor-/`
        helm3 upgrade --install $RELEASE_NAME -f $VALUES_PATH/$i . --namespace kube-monitoring
    done
fi